provider "aws" {
    region = "eu-west-3"
}

# security group

resource "aws_security_group" "my_sg" {
    name = "terraform-sg"
    description = "this is my sg"
    vpc_id = "vpc-0060b8832184f4688"
    ingress {
        protocol = "TCP"
        from_port = "22"
        to_port = "22"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "TCP"
        from_port = "80"
        to_port = "80"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        protocol = "-1"
        from_port = "0"
        to_port = "0"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

# variable

variable "vpc_id" {
    default = "vpc-0060b8832184f4688"
}

variable "image_id" {
    default = "ami-01d21b7be69801c2f"
}

variable "instance_type" {
    default = "t2.micro"
}

# instance 

resource "aws_instance" "my_inst" {
    ami = var.image_id
    instance_type = var.instance_type
    key_name = "riteish-deshmukh"
    vpc_security_group_ids = ["sg-0caf41f84749794dd", aws_security_group.my_sg.id]
    tags = {
        Name = "inst-terra"
        env = "dev"
    }

}